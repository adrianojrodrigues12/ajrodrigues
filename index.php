<?php 

    $dia = date("d");
    $mes = date("m");
    $ano = date("Y");

    
    $diaSemana = Array ("Segunda-feira", "Terça-feira", "Quarta-feira", "Quinta-feira", "Sexta-feira", "Sábado", "Domingo");

    $meses = Array ("Janeiro","Fevereiro","Março","Abril","Maio","Junho","Julho","Agosto","Setembro","Outubro","Novembro","Dezembro");
    echo $diaSemana[$dia-1] . ", " . $dia . " de " . $meses[$mes-1] . " de " . $ano;

?>

<!DOCTYPE html>
<html>
<head>
    <title>Adriano Rodrigues - Personal Website</title>
    
        <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
        
    <link rel="stylesheet" type="text/css" media="screen" href="/css/styles.css" />
    
        <script src="/js/main.js"></script>
</head>


<body>

    <?php
        include "../ajrodrigues/paginas/01-topo.php";
        include "../ajrodrigues/paginas/02-menu.php";
    ?>

</body>
</html>